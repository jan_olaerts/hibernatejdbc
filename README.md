#Mastermind
The entertaining game of Mastermind for all ages above 6

This is a basic/sample project 
for how to:
- write classes/units
- make unit tests using JUnit 

## This project uses
- Java 11
- JavaDoc
- Maven
- JUnit
- Git
