package be.multimedi.mastermind;

import be.multimedi.mastermind.consoleApp.entities.Level;
import be.multimedi.mastermind.consoleApp.tools.ConsolePrintTool;
import be.multimedi.mastermind.consoleApp.tools.StringTool;

import java.util.Random;

/**
 * This class is the logical main class
 * Makes and handles events
 * Defining game logic, stages, answer, guess
 * Checking them and responsible of the storing them.
 * @author Jan
 */
public class Mastermind {
   /**
    * Required variables for the games
    * correct answer, wrong answer, semi correct answer, guesses, game information
    * finishes statements
    */
   public static final String FULL_VALID_GUESS = "+";
   public static final String SEMI_VALID_GUESS = "-";
   static final String THE_GAME_FINISHED = "The game was already finished!";

   private String invalidGuessStructure;
   private int guesses;
   private String answer;
   private boolean finishedGame;
   private Level gameLevel;

   /**
    * getter for the gameLevel field
    * @return a Level object
    */
   public Level getGameLevel() {
      return gameLevel;
   }

   /**
    * setter for the gameLevel field
    * @param gameLevel is the value for the gameLevel field
    * @Throws IllegalArgumentException if gameLevel param is null
    */
   public void setGameLevel(Level gameLevel) throws IllegalArgumentException {
      if(gameLevel == null) throw new IllegalArgumentException("gameLevel cannot be null and must be an instance of Levels");
      this.gameLevel = gameLevel;
   }

   /**
    * getter for the answer field
    * @return the answer field
    */
   public String getAnswer() {
      return answer;
   }

   /**
    * setter for the answer field
    * @param answer value for the answer field
    * @Throws IllegalArgumentException if answer param is null
    */
   public void setAnswer(String answer) throws IllegalArgumentException {
      if(answer == null) throw new IllegalArgumentException("answer cannot be null");

      String inputStructureRegex =
              "^[" + getGameLevel().getLowNumber() + "-" + getGameLevel().getHighNumber() + "]{" + getGameLevel().getNumOfDigits() + "}$";

      if (!answer.matches(inputStructureRegex))
         throw new IllegalArgumentException("The answer must contain " + gameLevel.getNumOfDigits() +
                 " digits, from " + gameLevel.getLowNumber() + " to " + gameLevel.getHighNumber() + "!");

      this.answer = answer;
   }

   /**
    * getter for the finishedGame field
    * Checks if the game is finished or not
    * @return finishedGame field which is of type boolean
    */
   public boolean isFinishedGame() {
      return finishedGame;
   }

   /**
    * setter for the finishedGame field
    * @param finishedGame value for the finishedGame field
    */
   public void setFinishedGame(boolean finishedGame) {
      this.finishedGame = finishedGame;
   }

   /**
    * getter for the guesses field
    * @return the guesses field
    */
   public int getGuesses() {
      return guesses;
   }

   /**
    * setter for the guesses field
    * @param guesses value for the guesses field
    * @Throws IllegalArgumentException if guesses param is smaller than 0
    */
   public void setGuesses(int guesses) throws IllegalArgumentException {
      if(guesses < 0) throw new IllegalArgumentException("guesses cannot be negative");
      this.guesses = guesses;
   }

   /**
    * getter for the invalidGuessStructure field
    * @return the invalidGuessStructure field
    */
   public String getInvalidGuessStructure() {
      return invalidGuessStructure;
   }

   /**
    * setter for the invalidGuessStructure field
    * @param invalidGuessStructure is the value for the invalidGuessStructure field
    * @Throws IllegalArgumentException if invalidGuessStructure param is null
    */
   public void setInvalidGuessStructure(String invalidGuessStructure) throws IllegalArgumentException {
      if(invalidGuessStructure == null) throw new IllegalArgumentException("invalidGuessStructure cannot be null");

      this.invalidGuessStructure = invalidGuessStructure;
   }

   /**
    * Sets the finishedGame field to false
    * Sets the guesses field to 0
    * Calls createRandomAnswer(), which makes a new answer
    * @param gameLevel the Level object which needs to be reset
    * @Throws IllegalArgumentException if the gameLevel param is null
    */
   public void reset(Level gameLevel) throws IllegalArgumentException {
      if(gameLevel == null) throw new IllegalArgumentException("gameLevel cannot be null");

      finishedGame = false;
      guesses = 0;
      createRandomAnswer(gameLevel);
   }

   /**
    * Checks a guess from the user
    * @param guess the value of the user input
    * @param gameLevel the value of the selected Level object
    * @return String according to correct, semi-correct or false answer
    * @Throws IllegalArgumentException if guess param is null
    * @Throws IllegalArgumentException if gameLevel param is null
    * @Throws IllegalArgumentException if the guess param does not comply with the standards given by the Level object
    */
   public String checkGuess(String guess, Level gameLevel) throws IllegalArgumentException {

      // validation
      if(guess == null || gameLevel == null) throw new IllegalArgumentException("guess or gameLevel cannot be null");
      guess = guess.replaceAll("( )*", "");

      int lowNum = gameLevel.getLowNumber();
      int highNum = gameLevel.getHighNumber();
      int numDigits = gameLevel.getNumOfDigits();

      String inputStructureRegex = "^[" + lowNum + "-" + highNum + "]{" + numDigits + "}$";

      if (guess.length() < getAnswer().length()
              || guess.length() > getAnswer().length()
              || guess.length() == 0
              || !guess.matches(inputStructureRegex)) {
         throw new IllegalArgumentException(
                 "Guess must contain " + numDigits + " digits between " + lowNum +" and " + highNum);
      }

      setInvalidGuessStructure("Invalid: The guess needs to be " + numDigits +
              " digits, from " + lowNum + " to " + highNum + "!");

      if (isFinishedGame()) return THE_GAME_FINISHED;

      if (guess.isBlank()) return getInvalidGuessStructure();
      if (!guess.matches(inputStructureRegex)) return getInvalidGuessStructure();

      // actually check the guess
      setGuesses(getGuesses() + 1);

      String answer = getAnswer();
      String currentGuess = guess;
      StringBuilder correctPos = new StringBuilder();
      StringBuilder notCorrectPos = new StringBuilder();

      for(int i = 0; i < answer.length(); i++) {
         // Check match on the same position
         if(answer.charAt(i) == guess.charAt(i)) {
            correctPos.append(FULL_VALID_GUESS);
            answer = StringTool.setX(answer, i);

            if(gameLevel.isAllowDoubles()) {
               currentGuess = StringTool.setX(currentGuess, i);
            }
         }
      }

      for (int i = 0; i < answer.length(); i++) {
         if(answer.charAt(i) == 'x') continue;

         // Check matches for all positions
         for (int j = 0; j < currentGuess.length(); j++) {
            if (i == j) continue;

            if (answer.charAt(i) == currentGuess.charAt(j) && currentGuess.charAt(j) != 'x') {
               notCorrectPos.append(SEMI_VALID_GUESS);
               answer = StringTool.setX(answer, i);

               break; // Breaking out of guess loop since answer is all unique chars
            }
         }
      }

      String result = correctPos.toString() + notCorrectPos.toString();
      //# check if the game is finished
      if (result.matches("[" + FULL_VALID_GUESS + "]{" + numDigits + "}"))
         finishedGame = true;

      //# return result
      return result;
   }

   /**
    * Creating the random answer according to given gameLevel object's data
    * @param gameLevel the value needed for creating a new answer
    * @Throws IllegalArgumentException if the gameLevel param is null
    */
   public void createRandomAnswer(Level gameLevel) {
      if(gameLevel == null) throw new IllegalArgumentException("gameLevel cannot be null");

      int lowNum = gameLevel.getLowNumber();
      int highNum = gameLevel.getHighNumber();
      int numDigits = gameLevel.getNumOfDigits();

      Random rand = new Random();
      StringBuilder sb = new StringBuilder();
      int count = 0;

      do {
         String input;

         do {
            input = Integer.toString(rand.nextInt(highNum + 1));
         } while(!input.matches("[" + lowNum + "-" + highNum + "]"));

         if (!gameLevel.isAllowDoubles() && !sb.toString().contains(input)) {
            sb.append(input);
            count++;
         }

         if (gameLevel.isAllowDoubles()) {
            sb.append(input);
            count++;
         }

      } while (count < numDigits);

      setAnswer(sb.toString());
   }
}