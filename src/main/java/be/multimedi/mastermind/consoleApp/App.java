package be.multimedi.mastermind.consoleApp;

import be.multimedi.mastermind.*;
import be.multimedi.mastermind.consoleApp.entities.Level;
import be.multimedi.mastermind.exceptions.LevelException;
import be.multimedi.mastermind.consoleApp.entities.Score;
import be.multimedi.mastermind.consoleApp.tools.ConsoleInputTool;
import be.multimedi.mastermind.consoleApp.tools.ConsolePrintTool;
import be.multimedi.mastermind.exceptions.ScoreException;
import be.multimedi.mastermind.consoleApp.tools.HibernateTool;
import be.multimedi.mastermind.consoleApp.services.LevelService;
import be.multimedi.mastermind.consoleApp.services.ScoreService;

import java.util.List;

/**
 * Main application for mastermind game
 * @author Jan
 */
public class App {
    Mastermind mastermind = new Mastermind();
    ConsoleInputTool consoleInput = new ConsoleInputTool();
    int gamesPlayed = 0;
    Score scores = new Score();
    LevelService levelService = new LevelService();
    ScoreService scoreService = new ScoreService();
    String name;

    /**
     * Starts the application
     * @param args possible arguments given to the app
     * @throws LevelException if there is a problem with a Level object
     * @throws ScoreException if there is a problem with a Score object
     */
    public static void main(String[] args) throws LevelException, ScoreException {
        App app = new App();
        app.startGame();
        HibernateTool.closeEntityManagerFactory();
    }

    /**
     * Starts the game loop
     * Prints a bye message when player stops playing
     */
    public void startGame() throws LevelException, ScoreException {
        this.name = consoleInput.askUserName("Please enter your name: ");
        startMenu();

        System.out.println("Bye, " + name + "!");
    }

    /**
     * Determining the levels, taking all levels from database and
     * Asking to user a select a level
     *
     * @return int LevelId
     */
    public int determineLevel() {
        List<Level> allLevels = levelService.getAllLevels();
        int min = allLevels.stream().map(Level::getId).min(Integer::compare).orElse(0);
        int max = allLevels.stream().map(Level::getId).max(Integer::compare).orElse(0);
        if(max == 0) throw new LevelException("There are no levels in the db");
        int level;

        do {

            allLevels.forEach(l -> {
                String output = ConsolePrintTool.ANSI_YELLOW + l.getId() + ". " + l.getLevelName() + ConsolePrintTool.ANSI_RESET;
                if(l.getId() != allLevels.size()) System.out.print(output + " | ");
                else System.out.print(output);
            });

            System.out.println("\r");

            level = consoleInput.askUserPosIntBetweenRange("Give a level between " + min + " and " + max + ":", min, max);
            if (level < min || level > max) System.err.println("Level cannot be smaller than " + min + " or higher than " + max + ".");
        } while (level < min || level > max);

        return level;
    }

    /**
     * Setting the game level according to user input
     * @param levelId getting from user as which level wants to play
     */
    public void setLevelData(int levelId) {
        List<Level> allLevels = levelService.getAllLevels();
        int min = allLevels.stream().map(Level::getId).min(Integer::compare).orElse(0);
        int max = allLevels.stream().map(Level::getId).max(Integer::compare).orElse(0);
        if (levelId < min || levelId > max) throw new IllegalArgumentException("levelId must be between " + min + " and " + max + ".");

        mastermind.setGameLevel(levelService.getLevelById(levelId));
    }

    /**
     * Where the game functionally works
     * printing rules according to level
     * setting level, reset variables for new game, asking from user for guess, checking that guess and printing result
     * storing the score and pushing to the database
     */
    void runGame() throws LevelException, ScoreException {

        beforePlay();
        long startTime = System.currentTimeMillis();
        while (!mastermind.isFinishedGame()) {
            String guess;
            if (!mastermind.getGameLevel().isAllowDoubles()) {

                do {
                    guess = consoleInput.askSpecificAmountOfPosDigitsStringBetweenRange(
                            "Make a guess: ",
                            mastermind.getGameLevel().getNumOfDigits(), mastermind.getGameLevel().getLowNumber(), mastermind.getGameLevel().getHighNumber());
                } while (!consoleInput.checkNoDoublesInString(guess));

            } else {
                guess = consoleInput.askSpecificAmountOfPosDigitsStringBetweenRange(
                        "Make a guess: ",
                        mastermind.getGameLevel().getNumOfDigits(), mastermind.getGameLevel().getLowNumber(), mastermind.getGameLevel().getHighNumber());
            }

            System.out.println("Answer: " + mastermind.checkGuess(guess, mastermind.getGameLevel()));
        }

        won(startTime);
    }

    /**
     * Does the logic before a game starts
     */
    private void beforePlay() {

        setLevelData(determineLevel());
        mastermind.reset(mastermind.getGameLevel());
        ConsolePrintTool.printTitle("Welcome to Mastermind " + mastermind.getGameLevel().getLevelName() + " level!");
        scores = new Score();
        scores.setScoreLevelId(mastermind.getGameLevel().getId());
        scores.setPlayerName(name);
        if (!scoreService.getScoresByPlayerName(name).isEmpty()) {
            gamesPlayed = scoreService.getNumberOfGamesPlayedByPlayerName(name);
            scores.setGamesPlayed(gamesPlayed);
        } else {
            scores.setGamesPlayed(0);
        }

        System.out.println("I'm thinking of a " + mastermind.getGameLevel().getNumOfDigits() + " digit code. " +
                "Numbers starting from " + mastermind.getGameLevel().getLowNumber() + " up to " + mastermind.getGameLevel().getHighNumber() +
                ", duplicates" + (mastermind.getGameLevel().isAllowDoubles() ? " " : " not ") + "allowed.");

        System.out.println(Mastermind.FULL_VALID_GUESS + " represents a correct number guessed in the correct position.");
        System.out.println(Mastermind.SEMI_VALID_GUESS + " represents a correct number guessed in the wrong position.");
    }

    /**
     * Does the logic when the user won a game
     * @param startTime is the start time of the actual game
     */
    private void won(long startTime) {
        long endTime = System.currentTimeMillis();
        String duration = ConsolePrintTool.formatTime(endTime - startTime);
        System.out.println("Congratulations!");
        System.out.println("You guessed " + mastermind.getGuesses() + " times.");
        scores.setNumberOfGuesses(mastermind.getGuesses());
        scores.setTimePlayed(duration);
        gamesPlayed++;
        scores.setGamesPlayed(gamesPlayed);

        scoreService.addScore(scores);
        ConsolePrintTool.printHeading();
        System.out.println(scores);
    }

    /**
     * Prints the menu choices
     */
    private void printMenu() {
        System.out.println("1. Play.");
        System.out.println("2. New Player");
        System.out.println("3. Overview of HighScores.");
        System.out.println("4. View previous scores by playerName.");
        System.out.println("5. Get top 10 for each level.");
        System.out.println("6. Exit the app.");
        ConsolePrintTool.printEnter();
    }

    /**
     * According to selection from the menu
     * starting different methods
     * @throws ScoreException handling if there is a problem with a Score object
     */
    public void startMenu() throws ScoreException {
        int choice;
        do {
            System.out.println("Hello " + ConsolePrintTool.ANSI_YELLOW + name + ConsolePrintTool.ANSI_RESET + "!" + " Hope you are having a good day! Welcome to the starting menu," +
                    "please choose one of the following options: ");

            printMenu();

            choice = consoleInput.askUserPosIntBetweenRange("Input a number between 1 and 6", 1, 6);

            if (choice == 1) {
                do {
                    runGame();
                } while (consoleInput.askYesOrNo("Do you want to play again?(y/n): "));
                System.out.println(name + " played Mastermind " + gamesPlayed + " times.");
            }

            if (choice == 2) {
                this.name = consoleInput.askUserName("Please enter your name: ");
                 if (scoreService.getScoresByPlayerName(name).isEmpty()){
                     gamesPlayed = 0;
                     scores.setGamesPlayed(gamesPlayed);
                 }else if (!scoreService.getScoresByPlayerName(name).isEmpty()) {
                     gamesPlayed = scoreService.getNumberOfGamesPlayedByPlayerName(name);
                     scores.setGamesPlayed(gamesPlayed);
                 }
            }
            if (choice == 3) {

                List<Level> allLevels = levelService.getAllLevels();
                allLevels.forEach(l -> {
                    ConsolePrintTool.printHeading();
                    Score score = scoreService.getHighscorePerLevel(l.getId());
                    if(score != null) ConsolePrintTool.printBlue(score.toString());
                    else ConsolePrintTool.printBlue("There is no high score for level " + l.getId());
                    ConsolePrintTool.printEnter();
                });
                ConsolePrintTool.printEnter();
            }

            if (choice == 4) {
                String name = consoleInput.askUserName("Insert name: ");

                ConsolePrintTool.printHeading();
                List<Score> scores = scoreService.getScoresByPlayerName(name);
                if(scores != null && scores.size() > 0) scores.forEach(s -> ConsolePrintTool.printBlue(s.toString()));
                else ConsolePrintTool.printBlue("There are no scores for " + name);
                ConsolePrintTool.printEnter();
            }

            if (choice == 5) {

                List<Level> allLevels = levelService.getAllLevels();
                allLevels.forEach(l -> {
                    ConsolePrintTool.printEnter();
                    ConsolePrintTool.printHeading();
                    List<Score> scores = scoreService.getTenBestScoresPerLevel(l.getId());
                    if(scores != null && scores.size() > 0) scores.forEach(s -> ConsolePrintTool.printBlue(s.toString()));
                    else ConsolePrintTool.printBlue("There are no scores for level " + l.getId());
                });
                ConsolePrintTool.printEnter();
            }
        }
        while (choice != 6);
    }
}