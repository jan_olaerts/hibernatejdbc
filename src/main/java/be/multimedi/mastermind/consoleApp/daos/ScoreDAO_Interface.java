package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.entities.Score;
import be.multimedi.mastermind.exceptions.ScoreException;

import java.util.List;
/** Data Access Object Interface for MMScores Table
 * @author Jan
 * Defining the methods name which will using for DAO class
 */
public interface ScoreDAO_Interface {

    Score getScoreById(int id) throws ScoreException;
    void addScore(Score score) throws ScoreException;
    void updateScore(Score score) throws ScoreException;
    List<Score> getScoresByPlayerName(String playerName) throws ScoreException;
    Score getHighscorePerLevel(int scoreLevelId) throws ScoreException;
    List<Score> getTenBestScoresPerLevel(int scoreLevelId) throws ScoreException;
    int getNumberOfGamesPlayedByPlayerName(String playerName) throws ScoreException;
}