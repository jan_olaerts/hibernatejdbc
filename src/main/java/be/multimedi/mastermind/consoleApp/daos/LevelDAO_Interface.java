package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.entities.Level;
import be.multimedi.mastermind.exceptions.LevelException;

import java.util.List;

/** Data Access Object Interface for MMLevels Table
 * @author Jan
 * Defining the methods name which are used in the LevelDAO class
 */
public interface LevelDAO_Interface {
    List<Level> getAllLevels() throws LevelException;
    Level getLevelById(int id) throws LevelException;
}