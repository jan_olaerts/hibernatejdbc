package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.entities.Level;
import be.multimedi.mastermind.consoleApp.tools.HibernateTool;
import be.multimedi.mastermind.exceptions.LevelException;

import javax.persistence.*;
import java.util.List;

/** Data Access Object for Levels Table
* @author Jan
* Listing queries for Level entity with Hibernate
 */
public class LevelDAO implements LevelDAO_Interface {

    /**
     * Getting from Levels table the whole information
     * @return the Levels list of the levels
     * @throws LevelException if there is a problem with the query
     */
    @Override
    public List<Level> getAllLevels() throws LevelException {

        List<Level> levels = null;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            TypedQuery<Level> query = em.createNamedQuery("getAllLevels", Level.class);
            levels = query.getResultList();
        } catch (RuntimeException re) {
            System.out.println("Exception in getAllLevels()");
        } finally {
            HibernateTool.closeEntityManager();
        }

        return levels;
    }

    /**
     * Gathering the data from Levels according the given Id
     * @param id is the level primary key in the MMLevels table
     * @return a Level object which contains the specific information according the id param
     * @Throws LevelException if there is a problem with the query
     */
    @Override
    public Level getLevelById(int id) throws LevelException {
        if(id < 0) throw new LevelException("id cannot be negative");

        Level level = null;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            TypedQuery<Level> query =
                    em.createNamedQuery("getLevelById", Level.class);
            query.setParameter(1, id);

            level = query.getSingleResult();

        } catch (RuntimeException re) {
            System.out.println("Exception in getLevelById()");
            re.printStackTrace();
        } finally {
            HibernateTool.closeEntityManager();
        }

        return level;
    }

    /**
     * Adding a new Level to Levels table
     * @param level is an object that contains the required information for database
     * @Throws LevelException if there is a problem with the query
     */

    public void addLevel(Level level) throws LevelException {
        if(level == null) throw new LevelException("level cannot be null");

        try {
            EntityManager em = HibernateTool.getEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(level);
            tx.commit();
        } catch (RuntimeException re) {
            System.out.println("Exception in addLevel() " + re.getMessage());
        } finally {
            HibernateTool.closeEntityManager();
        }
    }
}