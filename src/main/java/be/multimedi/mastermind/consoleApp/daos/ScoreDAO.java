package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.entities.Score;
import be.multimedi.mastermind.exceptions.ScoreException;
import be.multimedi.mastermind.consoleApp.tools.HibernateTool;

import javax.persistence.*;
import java.util.List;

/** Data Access Object for Scores Table
 * @author Jan
 * Listing queries for Score entity with Hibernate
 */
public class ScoreDAO implements ScoreDAO_Interface {

    /**
     * Getting Scores according to the given Id
     * @param id the primary key of the Scores table
     * @return a Score object
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public Score getScoreById(int id) throws ScoreException {
        if (id < 0) throw new ScoreException("id cannot be negative");

        Score score = null;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            TypedQuery<Score> query =
                    em.createNamedQuery("getScoreById", Score.class);
            query.setParameter(1, id);

            score = query.getSingleResult();

        } catch (RuntimeException re) {
            System.out.println("Exception in getScoreById()");
        } finally {
            HibernateTool.closeEntityManager();
        }

        return score;
    }

    /**
     * Getting the data of the each level high scores from the Score table
     * @param scoreLevelId defining the level
     * @return a Score Object that contains the high score of that level
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public Score getHighscorePerLevel(int scoreLevelId) throws ScoreException {
        if (scoreLevelId < 0) throw new ScoreException("scoreLevelId cannot be negative");

        Score score = null;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            TypedQuery<Score> query =
                    em.createNamedQuery("getHighscorePerLevel", Score.class);
            query.setParameter(1, scoreLevelId);
            query.setMaxResults(1);
            score = query.getSingleResult();

        } catch (RuntimeException ignored) {

        } finally {
            HibernateTool.closeEntityManager();
        }

        return score;
    }

    /**
     * Adding a Score to the Scores table
     * @param score object that contains the required information for database
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public void addScore(Score score) throws ScoreException {
        if(score == null) throw new IllegalArgumentException("score cannot be null");

        try {
            EntityManager em = HibernateTool.getEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.persist(score);
            tx.commit();

        } catch (RuntimeException re) {
            System.out.println("Exception in addScore()");
        } finally {
            HibernateTool.closeEntityManager();
        }
    }

    /**
     * Updating a score object in the Scores table
     * @param score object contains the fields to be updated
     * @throws ScoreException if there is a problem
     */
    @Override
    public void updateScore(Score score) throws ScoreException {
        if(score == null) throw new IllegalArgumentException("score cannot be null");

        try {
            EntityManager em = HibernateTool.getEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            em.merge(score);
            tx.commit();
        } catch (RuntimeException re) {
            System.out.println("Error in updateScore()");
        } finally {
            HibernateTool.closeEntityManager();
        }
    }

    /**
     * Getting the information according the Player name from the Scores table
     * @param playerName for selecting the specific row from Scores table
     * @return a list that contains all the scores of the given player
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public List<Score> getScoresByPlayerName(String playerName) throws ScoreException {
        if (playerName == null) throw new ScoreException("playerName cannot be null");

        List<Score> scores = null;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            TypedQuery<Score> query =
                    em.createNamedQuery("getScoresByPlayerName", Score.class);
            query.setParameter(1, playerName);
            scores = query.getResultList();

        } catch (RuntimeException re) {
            System.out.println("Exception in getScoresByPlayerName()");
        } finally {
            HibernateTool.closeEntityManager();
        }

        return scores;
    }

    /**
     * Getting the 10 best scores from the database according to Level
     * @param scoreLevelId defining the level
     * @return a list of Score objects which contains the highest 10 scores
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public List<Score> getTenBestScoresPerLevel(int scoreLevelId) throws ScoreException {
        if (scoreLevelId < 0) throw new ScoreException("scoreLevelId cannot be negative");

        List<Score> scores = null;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            TypedQuery<Score> query =
                    em.createNamedQuery("getTenBestScoresPerLevel", Score.class);
            query.setParameter(1, scoreLevelId);
            query.setFirstResult(0);
            query.setMaxResults(9);
            scores = query.getResultList();

        } catch (RuntimeException re) {
            System.out.println("Exception in getTenBestScoresPerLevel()");
        } finally {
            HibernateTool.closeEntityManager();
        }

        return scores;
    }

    /**
     * Checks the number of games played by player name
     * @param playerName entered username
     * @return an int which is the number of games played by the player name
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public int getNumberOfGamesPlayedByPlayerName(String playerName) throws ScoreException {
        if(playerName == null) throw new ScoreException("Invalid playername");

        List<Integer> totalTimesPlayedList;
        int gamesPlayed = 0;

        try {
            EntityManager em = HibernateTool.getEntityManager();
            TypedQuery<Integer> query =
                    em.createNamedQuery("getTotalGamesPlayedByPlayerName", Integer.class);
            query.setParameter(1, playerName);
            query.setMaxResults(1);

            totalTimesPlayedList = query.getResultList();
            if(totalTimesPlayedList.size() > 0) gamesPlayed = totalTimesPlayedList.get(0);

        } catch (RuntimeException re) {
            System.out.println("Exception in getNumberOfGamesPlayedByPlayerName()");
            System.out.println(re.getMessage());
        } finally {
            HibernateTool.closeEntityManager();
        }

        return gamesPlayed;
    }
}