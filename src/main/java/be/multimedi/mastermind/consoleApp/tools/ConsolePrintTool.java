package be.multimedi.mastermind.consoleApp.tools;

import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * ConsolePrintTool
 * For special console printing needs
 * @version 2.0.1 , 10-may-2020
 */
public final class ConsolePrintTool {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    private static final String ANSI_BLUE = "\u001B[34m";

    /**
     * Concats the same amount of given characters to a given string as there are character in the string
     * @param title is the string
     * @param type is the character to be added to the string
     */
    public static void printTitle(String title, char type) {
        System.out.println(title);
        System.out.println(String.valueOf(type).repeat(title.length()));
    }

    /**
     * Calls the printTitle(String title, char type) method with '-' as type
     * @param title is the string to be altered
     */
    public static void printTitle(String title) {
        printTitle(title, '-');
    }

    /**
     * Formats a given long value to a date string
     * @param millis is the long value from 1st january 1970
     * @return string value with human-readable form of the date
     */
    public static String formatTime(long millis) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(millis);
    }

    /**
     * Prints the heading for showing scores
     */
    public static void printHeading() {
        System.out.println(ANSI_YELLOW + String.format("%10s| %9s| %8s| %10s| %10s| %6s",
                "Name", "#Guesses", "#Played", "#PlayedTotal","Playtime", "Level") + ANSI_RESET);
        System.out.println("--------------------------------------------------------------------------------");
    }

    /**
     * Prints a given string in blue
     * @param text string to be printed in blue
     */
    public static void printBlue(String text) {
        System.out.println(ANSI_BLUE + text + ANSI_RESET);
    }

    /**
     * Prints an enter
     */
    public static void printEnter() {
        System.out.println("\r");
    }
}