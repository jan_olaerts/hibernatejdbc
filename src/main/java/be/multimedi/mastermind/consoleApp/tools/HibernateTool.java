package be.multimedi.mastermind.consoleApp.tools;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Class to do Hibernate-specific actions
 */
public class HibernateTool {

    static EntityManagerFactory emf = null;
    private static EntityManager em = null;

    /**
     * Gets an EntityManager object
     * @return an EntityManager object
     */
    public static EntityManager getEntityManager() {

        try {

            if(emf == null) {
                emf = Persistence.createEntityManagerFactory("course");
            }

            em = emf.createEntityManager();

        } catch (RuntimeException re) {
            System.out.println("Could not create entity manager");
            re.printStackTrace();
        }

        return em;
    }

    /**
     * Closes the EntityManager object
     */
    public static void closeEntityManager() {

        if(em != null) {
            em.close();
            em = null;
        }
    }

    /**
     * Closes the EntityManagerFactory object
     */
    public static void closeEntityManagerFactory() {

        if(emf != null) {
            emf.close();
            emf = null;
        }
    }
}