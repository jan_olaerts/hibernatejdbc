package be.multimedi.mastermind.consoleApp.entities;

import be.multimedi.mastermind.exceptions.LevelException;

import javax.persistence.*;

/** This is the entity class for the Levels table
 * and storing them in a this class object
 * @author Jan
 */

@Entity
@Table(name="Levels")
@NamedQueries(value = {
        @NamedQuery(name="getAllLevels", query="select l from Level l"),
        @NamedQuery(name="getLevelById", query="select l from Level l where l.id=?1"),
})
public class Level {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String levelName;
    private int lowNumber;
    private int highNumber;
    private int numOfDigits;
    private boolean allowDoubles;

    /**
     * No-arg constructor for Level entity class
     */
    public Level() {
    }

    /**
     * Level entity class constructor with parameters
     * @param levelName is a string type variable
     * @param lowNumber is a int type variable
     * @param highNumber is a int type variable
     * @param numOfDigits is a int type variable
     * @param allowDoubles is a boolean type variable
     */
    public Level(String levelName, int lowNumber, int highNumber, int numOfDigits, boolean allowDoubles) {

        setLevelName(levelName);
        setHighNumber(highNumber);
        setLowNumber(lowNumber);
        setNumOfDigits(numOfDigits);
        setAllowDoubles(allowDoubles);
    }

    /**
     * getter for variable Id
     * @return int type variable which is the id
     */
    public int getId() {
        return id;
    }


    /**
     * setter for id field
     * @param id is giving number for setting the Id variable
     * @Throws LevelException if id param is null or smaller than 0
     */
    public void setId(Integer id) throws LevelException {
        if(id != null && id < 0) throw new LevelException("id cannot be negative");
        this.id = id;
    }

    /**
     * getter for the levelName field
     * @return a string variable that contains levelName
     */
    public String getLevelName() {
        return levelName;
    }

    /**
     * setter for the levelName variable
     * @param levelName is a string for setting levelName
     * @Throws LevelException if levelName param is null
     */
    public void setLevelName(String levelName) throws LevelException {
        if(levelName == null) throw new LevelException("levelName cannot be null");
        this.levelName = levelName;
    }

    /**
     * getter for the lowNumber field
     * @return int type variable which contains the lowNumber
     */
    public int getLowNumber() {
        return lowNumber;
    }

    /**
     * setter for the lowNumber field
     * @param lowNumber is the value for setting the lowNumber field
     * @Throws LevelException if lowNumber param is smaller than null
     * @Throws LevelException if lowNumber param is higher than highNumber field
     */
    public void setLowNumber(int lowNumber) throws LevelException {
        if(lowNumber < 0) throw new LevelException("lowNumber cannot be negative");
        if(lowNumber > highNumber) throw new LevelException("lowNumber cannot be higher than highNumber");
        this.lowNumber = lowNumber;
    }

    /**
     * getter for the highNumber field value
     * @return int type variable that contains highNumber variable's value
     */
    public int getHighNumber() {
        return highNumber;
    }

    /**
     *  setter for the highNumber field
     * @param highNumber is the value for setting the highNumber field
     * @Throws LevelException if the highNumber param is smaller than the lowNumber field
     */
    public void setHighNumber(int highNumber) throws LevelException {
        if(highNumber < lowNumber) throw new LevelException("highNumber cannot be less than lowNumber");
        this.highNumber = highNumber;
    }

    /**
     * getter for the numOfDigits field
     * @return int type value of numOfDigits field
     */
    public int getNumOfDigits() {
        return numOfDigits;
    }

    /**
     * setter for the numOfDigits field
     * @param numOfDigits is a value for setting the numOfDigits field
     * @Throws LevelException if numOfDigits param is smaller than 1
     */
    public void setNumOfDigits(int numOfDigits) throws LevelException {
        if(numOfDigits < 1) throw new LevelException("numOfDigits cannot be smaller than 1");
        this.numOfDigits = numOfDigits;
    }

    /**
     * getter for the allowDoubles field
     * @return boolean value of the allowDoubles field
     */
    public boolean isAllowDoubles() {
        return allowDoubles;
    }

    /**
     * setter for the allowDoubles field
     * @param allowDoubles is a boolean value for setting the allowDoubles field
     */
    public void setAllowDoubles(boolean allowDoubles) throws LevelException {
        this.allowDoubles = allowDoubles;
    }

    /**
     * @return a String object with all the fields of the Level object
     */
    @Override
    public String toString() {
        return "Level{" +
                "id=" + id +
                ", levelName='" + levelName + '\'' +
                ", lowNumber=" + lowNumber +
                ", highNumber=" + highNumber +
                ", numOfDigits=" + numOfDigits +
                ", allowDoubles=" + allowDoubles +
                '}';
    }
}