package be.multimedi.mastermind.consoleApp.entities;

import be.multimedi.mastermind.consoleApp.services.ScoreService;
import be.multimedi.mastermind.exceptions.ScoreException;

import javax.persistence.*;
import java.util.Objects;
/** This is the entity class for the Scores table
 * @author Jan
 */
@Entity
@Table(name="Scores")
@NamedQueries(value = {
        @NamedQuery(name="getScoreById", query="select s from Score s where s.id=?1"),

        @NamedQuery(name="getHighscorePerLevel", query="select s from Score s where s.scoreLevelId=?1" +
                "order by s.scoreLevelId, s.numberOfGuesses, s.timePlayed"),

        @NamedQuery(name="getScoresByPlayerName", query="select s from Score s where s.playerName=?1"),
        @NamedQuery(name="getTenBestScoresPerLevel", query="select s from Score s where s.scoreLevelId=?1 " +
                "order by s.numberOfGuesses, s.timePlayed asc"),

        @NamedQuery(name= "getTotalGamesPlayedByPlayerName", query="select s.gamesPlayed from Score s where s.playerName=?1 " +
                "order by s.gamesPlayed desc")

})
public class Score {

    /**
     * Variables which are the same type of the Database and same name declaration
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String playerName;
    private int numberOfGuesses;
    private int gamesPlayed;
    private String timePlayed;
    private Integer scoreLevelId;

    /**
     * No-arg constructor for Score entity class
     */
    public Score() {
    }


    /**
     * Score entity class constructor with parameters
     * @param playerName is a string type variable
     * @param numberOfGuesses is an int type variable
     * @param gamesPlayed is an int type variable
     * @param timePlayed is a long type variable
     * @param scoreLevelId is an int type variable
     *
     */
    public Score(String playerName, int numberOfGuesses, int gamesPlayed, String timePlayed, Integer scoreLevelId) {

        setPlayerName(playerName);
        setNumberOfGuesses(numberOfGuesses);
        setGamesPlayed(gamesPlayed);
        setTimePlayed(timePlayed);
        setScoreLevelId(scoreLevelId);
    }

    /**
     * getter for id field
     * @return int type variable which is id
     */
    public int getId() {
        return id;
    }
    /**
     * setter for the id field
     * @param id is giving number for setting the id field
     * @Throws ScoreException if id param is null or smaller than 0
     */
    public void setId(Integer id) throws ScoreException {
        if (id != null && id < 0) throw new ScoreException("id cannot be null or negative");
        this.id = id;
    }
    /**
     * Getting the playerName variable
     * @return a string variable that contains playerName.
     */
    public String getPlayerName() {
        return playerName;
    }
    /**
     * setter for the playerName field
     * @param playerName is a string for setting the playerName field
     * @Throws ScoreException if playerName param is null or blank
     */
    public void setPlayerName(String playerName) throws ScoreException {
        if (playerName == null || playerName.isBlank()) throw new ScoreException("playerName cannot be null or empty");
        this.playerName = playerName;
    }

    /**
     * getter for numberOfGuesses field
     * @return int type value of numberOfGuesses
     */
    public int getNumberOfGuesses() { return numberOfGuesses; }


    /**
     * setter for the numberOfGuessed field
     * @param numberOfGuesses is value for the assign the numberOfDigits field
     * @Throws ScoreExcpetion if the numberOfGuesses param is smaller than 0
     */
    public void setNumberOfGuesses(int numberOfGuesses) throws ScoreException {
        if(numberOfGuesses < 0) throw new ScoreException("numberOfGuesses cannot be null or negative");

        this.numberOfGuesses = numberOfGuesses;
    }

    /**
     * getter for the gamesPlayed field
     * @return an int type value of the gamesPlayed field
     */
    public int getGamesPlayed() {
        return gamesPlayed;
    }

    /**
     * setter for the gamesPlayed field
     * @param gamesPlayed is a value for assign the gamesPlayed field
     * @Throws ScoreException if the gamesPlayed param is smaller than 0
     */
    public void setGamesPlayed(int gamesPlayed) throws ScoreException {
        if(gamesPlayed < 0) throw new ScoreException("gamesPlayed cannot be null or negative");
        this.gamesPlayed = gamesPlayed;
    }

    /**
     * getter for the timePlayed field
     * @return a long value of timePlayed field
     */
    public String getTimePlayed() {
        return timePlayed;
    }

    /**
     * setter for the timePlayed field
     * @param timePlayed is a value for assign the timePlayed field
     * @Throws ScoreException if timePlayed param is null or blank
     */
    public void setTimePlayed(String timePlayed) throws ScoreException {
        if(timePlayed == null || timePlayed.isBlank()) throw new ScoreException("timePlayed cannot be null or blank");

        this.timePlayed = timePlayed;
    }

    /**
     * getter for the scoreLevelId field
     * @return int type value of the variable
     */
    public Integer getScoreLevelId() {
        return scoreLevelId;
    }


    /**
     * setter for the scoreLevelId field
     * @param scoreLevelId is a value for the assign the scoreLevelId field
     * @Throws ScoreException if scoreLevelId is null or smaller than 0
     */
    public void setScoreLevelId(Integer scoreLevelId) throws ScoreException {
        if(scoreLevelId == null || scoreLevelId < 0) throw new ScoreException("scoreLevelId cannot be null or negative");
        this.scoreLevelId = scoreLevelId;
    }

    /**
     * method for comparing two objects
     * @param o is an object that need to compare with our object
     * @return a true or false according the two object is equal or not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Score score = (Score) o;
        return id.equals(score.id) &&
                numberOfGuesses == score.numberOfGuesses &&
                gamesPlayed == score.gamesPlayed &&
                scoreLevelId == score.scoreLevelId &&
                Objects.equals(playerName, score.playerName) &&
                Objects.equals(timePlayed, score.timePlayed);
    }

    /**
     * getting the hash code of the object
     * @return int value of hashed object values
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId);
    }
/**
 * Method using for turning to the string values of object fields.
 */
    @Override
    public String toString() {
        ScoreService scoreService = new ScoreService();
        int totalGamesPlayed = scoreService.getNumberOfGamesPlayedByPlayerName(getPlayerName());
        return String.format("%10s| %9d| %8d| %12s| %10s| %6d",
                playerName, numberOfGuesses, gamesPlayed, totalGamesPlayed, timePlayed, scoreLevelId);
    }
}