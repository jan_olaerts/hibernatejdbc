package be.multimedi.mastermind.consoleApp.services;

import be.multimedi.mastermind.consoleApp.entities.Level;
import be.multimedi.mastermind.exceptions.LevelException;

import java.util.List;

public interface LevelService_Interface {
    List<Level> getAllLevels() throws LevelException;
    Level getLevelById(int id) throws LevelException;
}