package be.multimedi.mastermind.consoleApp.services;

import be.multimedi.mastermind.consoleApp.daos.LevelDAO;
import be.multimedi.mastermind.consoleApp.entities.Level;
import be.multimedi.mastermind.exceptions.LevelException;

/**
 * Level service for an extra level between the app and the database
 */
import java.util.List;

public class LevelService implements LevelService_Interface {

    private static final LevelDAO LEVEL_DAO = new LevelDAO();

    /**
     * Gets all the rows from the Levels table
     * @return a list of Level objects
     * @throws LevelException if there is a problem with the query
     */
    @Override
    public List<Level> getAllLevels() throws LevelException {
        return LEVEL_DAO.getAllLevels();
    }

    /**
     * Gets a Level object by its id
     * @param id is the id to retrieve the Level object by
     * @return a Level object
     * @throws LevelException if there is a problem with the query
     */
    @Override
    public Level getLevelById(int id) throws LevelException {
        return LEVEL_DAO.getLevelById(id);
    }
}