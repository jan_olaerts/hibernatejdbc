package be.multimedi.mastermind.consoleApp.services;

import be.multimedi.mastermind.consoleApp.daos.ScoreDAO;
import be.multimedi.mastermind.consoleApp.entities.Score;
import be.multimedi.mastermind.exceptions.ScoreException;
import java.util.List;

/**
 * Score service for an extra level between the app and the database
 */
public class ScoreService implements ScoreService_Interface {

    private static final ScoreDAO SCORE_DAO = new ScoreDAO();

    /**
     * Gets a Score object by its id
     * @param id is the id to retrieve the Score object by
     * @return a Score object
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public Score getScoreById(int id) throws ScoreException {
        return SCORE_DAO.getScoreById(id);
    }

    /**
     * Adds a Score object to the Scores table
     * @param score is the Score object to be added
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public void addScore(Score score) throws ScoreException {
        SCORE_DAO.addScore(score);
    }

    /**
     * Updates a row in the Scores table
     * @param score is the Score object to be updated
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public void updateScore(Score score) throws ScoreException {
        SCORE_DAO.updateScore(score);
    }

    /**
     * Gets a list of scores by the name of the player
     * @param playerName is the variable to select the Score objects by
     * @return a list of Score objects
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public List<Score> getScoresByPlayerName(String playerName) throws ScoreException {
        return SCORE_DAO.getScoresByPlayerName(playerName);
    }

    /**
     * Gets the high score for a given level
     * @param scoreLevelId is the level id column in the Scores table
     * @return a Score object
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public Score getHighscorePerLevel(int scoreLevelId) throws ScoreException {
        return SCORE_DAO.getHighscorePerLevel(scoreLevelId);
    }

    /**
     * Gets the ten best scores for a given level
     * @param scoreLevelId is the level id column in the Scores table
     * @return a list of Score objects
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public List<Score> getTenBestScoresPerLevel(int scoreLevelId) throws ScoreException {
        return SCORE_DAO.getTenBestScoresPerLevel(scoreLevelId);
    }

    /**
     * Checks the number of games player by player name
     * @param playerName is the player by which needs to be checked
     * @return an int which is the number of games played by the player name
     * @throws ScoreException if there is a problem with the query
     */
    @Override
    public int getNumberOfGamesPlayedByPlayerName(String playerName) throws ScoreException {
        return SCORE_DAO.getNumberOfGamesPlayedByPlayerName(playerName);
    }
}