package be.multimedi.mastermind.exceptions;
/**
 * Creating new class for handling the exceptions according to class
 */
public class LevelException extends RuntimeException {

    /**
     * Constructor for a LevelException object
     * @param message is the message for the LevelException object
     */
    public LevelException(String message) {
        super(message);
    }
}