package be.multimedi.mastermind.exceptions;
/**
 * Creating new class for handling the exceptions according to class
 */
public class ScoreException extends RuntimeException{

    /**
     * Constructor for a ScoreException object
     * @param message is the message for the ScoreException object
     */
    public ScoreException(String message) {
        super(message);
    }
}