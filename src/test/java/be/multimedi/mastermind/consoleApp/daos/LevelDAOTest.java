package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.entities.Level;
import be.multimedi.mastermind.consoleApp.tools.HibernateTool;
import be.multimedi.mastermind.exceptions.LevelException;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Test Class for Levels Data Access Object
 * Testing the methods according to critical points that could be cause an exception or a bug
 * @author Jan
 */
public class LevelDAOTest {

    static EntityManager em;
    static LevelDAO levelDAO;

    /**
     * Before All test making to link the test database
     */
    @BeforeAll
    static void getLevelDAO() {
        levelDAO = new LevelDAO();
    }

    /**
     * Before each method this code runs and rebuild the database as default for clean testing.
     * Creates tables, inserts default entries and linking Foreign Keys
     */
    @BeforeEach
    void init() {
        em = HibernateTool.getEntityManager();
        Session session = (Session) em.getDelegate();

        session.doWork(connection -> {

            try {
                ScriptRunner scriptRunner = new ScriptRunner( connection );

                scriptRunner.setLogWriter(null);

                BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/alterScores.sql"));
                scriptRunner.runScript(bufferedReader);

            } catch (FileNotFoundException fnfe) {
                System.out.println("File not found");
            }

        });
    }

    @AfterEach
    public void closeEntityManager() {
        em.clear();
        em.close();
    }

    /**
     * Test 1
     * Checking the getAllLevels method which returns a List of the Levels and checking the size of it
     */
    @Test
    void testGetAllLevels() throws LevelException {
        assertEquals(3, levelDAO.getAllLevels().size());

        Level testLevel = new Level("Difficult", 3, 8, 5, true);
        levelDAO.addLevel(testLevel);
        assertEquals(4, levelDAO.getAllLevels().size());

        Level testLevel2 = new Level("Difficult", 2, 8, 5, true);
        levelDAO.addLevel(testLevel2);
        assertEquals(5, levelDAO.getAllLevels().size());

        assertEquals("Normal", levelDAO.getLevelById(1).getLevelName());
    }

    /**
     *Test 2
     * Checking the getting method by Id and comparing it with our manuel created object
     */
    @Test
    void testGetLevelById() throws LevelException {
        Level dbLevel = levelDAO.getLevelById(1);
        assertEquals(1, dbLevel.getId());
        assertEquals("Normal", dbLevel.getLevelName());
        assertEquals(1, dbLevel.getLowNumber());
        assertEquals(6, dbLevel.getHighNumber());
        assertEquals(4, dbLevel.getNumOfDigits());
        assertFalse(dbLevel.isAllowDoubles());

        Level testLevel = new Level("Difficult", 6, 8, 5, true);
        levelDAO.addLevel(testLevel);

        Level dbLevel2 = levelDAO.getLevelById(4);
        assertEquals(testLevel.getId(), dbLevel2.getId());
        assertEquals(testLevel.getLevelName(), dbLevel2.getLevelName());
        assertEquals(testLevel.getLowNumber(), dbLevel2.getLowNumber());
        assertEquals(testLevel.getHighNumber(), dbLevel2.getHighNumber());
        assertEquals(testLevel.getNumOfDigits(), dbLevel2.getNumOfDigits());
        assertEquals(testLevel.isAllowDoubles(), dbLevel2.isAllowDoubles());
    }

    /**
     * Test 3
     * Checking the adding Level method to the database
     */
    @Test
    void testAddLevel() throws LevelException {
        assertEquals(3, levelDAO.getAllLevels().size());

        int amountOfRows = levelDAO.getAllLevels().size();
        Level levelToAdd = new Level("Super Difficult", 4, 9, 50, false);
        levelDAO.addLevel(levelToAdd);
        assertEquals(amountOfRows + 1, levelDAO.getAllLevels().size());
    }
}