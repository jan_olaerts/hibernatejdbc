package be.multimedi.mastermind.consoleApp.daos;

import be.multimedi.mastermind.consoleApp.entities.Score;
import be.multimedi.mastermind.consoleApp.tools.HibernateTool;
import be.multimedi.mastermind.exceptions.ScoreException;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.hibernate.Session;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Test Class for MMScores Data Access Object
 * Testing the methods according to critical points that could be cause a exception or a bug
 * @author Jan
 */
class ScoreDAOTest {

    static EntityManager em;
    static ScoreDAO scoreDAO;

    @BeforeAll
    static void getScoreDAO() {
        scoreDAO = new ScoreDAO();
    }
    /**
     * Before each method this code runs and rebuild the database as default for clean testing.
     * Creates tables, inserts default entries and linking Foreign Keys
     */
    @BeforeEach
    void init() {

        em = HibernateTool.getEntityManager();
        Session session = (Session) em.getDelegate();

        session.doWork(connection -> {

            try {
                ScriptRunner scriptRunner = new ScriptRunner( connection );

                scriptRunner.setLogWriter(null);

                BufferedReader bufferedReader = new BufferedReader(new FileReader("src/test/resources/createTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/insertTables.sql"));
                scriptRunner.runScript(bufferedReader);

                bufferedReader = new BufferedReader(new FileReader("src/test/resources/alterScores.sql"));
                scriptRunner.runScript(bufferedReader);

            } catch (FileNotFoundException fnfe) {
                System.out.println("File not found");
            }

        });
    }

    /**
     * Test 1
     *
     * Testing to getting Scores according to the Id and comparing them with our manuel given object if the results true or not.
     */
    @Test
    void testGetScoreById() throws ScoreException {
        Score testScore = new Score("Jan", 5, 1, "00:01:17", 3);
        scoreDAO.addScore(testScore);
        scoreDAO.addScore(testScore);
        Score dbScore = scoreDAO.getScoreById(11);

        assertEquals(testScore.getId(), dbScore.getId());
        assertEquals(testScore.getPlayerName(), dbScore.getPlayerName());
        assertEquals(testScore.getNumberOfGuesses(), dbScore.getNumberOfGuesses());
        assertEquals(testScore.getGamesPlayed(), dbScore.getGamesPlayed());
        assertEquals(testScore.getTimePlayed(), dbScore.getTimePlayed());
        assertEquals(testScore.getScoreLevelId(), dbScore.getScoreLevelId());
    }

    /**
     * Test 2
     * Checking the AddMMScores method with comparing our manual object and gathered object are equal or not
     *
     */
    @Test
    void testAddScore() throws ScoreException {
        Score testScore = new Score("Jan", 20, 20, "00:01:00", 2);

        scoreDAO.addScore(testScore);
        Score dbTestScore = scoreDAO.getScoreById(11);

        assertEquals(11, dbTestScore.getId());
        assertEquals("Jan", dbTestScore.getPlayerName());
        assertEquals(20, dbTestScore.getNumberOfGuesses());
        assertEquals(20, dbTestScore.getGamesPlayed());
        assertEquals("00:01:00", dbTestScore.getTimePlayed());
        assertEquals(Integer.valueOf(2), dbTestScore.getScoreLevelId());
    }

    /**
     * Test 3
     * Checking the update scores method with first given manuel scores for update
     * then gathering it and comparing them are they equal or not.
     */
    @Test
    void testUpdateScore() throws ScoreException {

        Score updateScore = new Score("Updated Player", 100, 100, "00:01:00", 1);
        updateScore.setId(1);

        scoreDAO.updateScore(updateScore);
        Score testScore = scoreDAO.getScoreById(1);
        assertEquals(updateScore, testScore);
        assertEquals("Updated Player", testScore.getPlayerName());
    }

    /**
     * Test 4
     * the getScoresByPlayerName method returns a list of Scores
     * This test is checking it according to expected size or not
     */
    @Test
    void testGetScoresByPlayerName() throws ScoreException {
        assertEquals(4, scoreDAO.getScoresByPlayerName("Player 4").size());
        assertEquals(2, scoreDAO.getScoresByPlayerName("Player 2").size());
        assertEquals(1, scoreDAO.getScoresByPlayerName("Player 1").size());
        assertNotEquals(1, scoreDAO.getScoresByPlayerName("Player 3").size());
    }

    /**
     * Test 5
     * Checking getHighScoresPerLevel method works properly or not
     */
    @Test
    void testGetHighscorePerLevel() throws ScoreException {
        Score highscoreLevel1 = scoreDAO.getHighscorePerLevel(1);
        Score highscoreLevel2 = scoreDAO.getHighscorePerLevel(2);
        Score highscoreLevel3 = scoreDAO.getHighscorePerLevel(3);

        assertEquals(scoreDAO.getScoreById(3), highscoreLevel1 );
        assertEquals(scoreDAO.getScoreById(10), highscoreLevel2 );
        assertEquals(scoreDAO.getScoreById(8), highscoreLevel3 );
    }

    /**
     * Test 6
     * Checking the getting the 10 best scores according per-level
     */
    @Test
    void testGetTenBestScoresPerLevel() throws ScoreException {
        assertEquals(1, scoreDAO.getTenBestScoresPerLevel(1).size());
        assertEquals(2, scoreDAO.getTenBestScoresPerLevel(2).size());
        assertEquals(7, scoreDAO.getTenBestScoresPerLevel(3).size());

        List<Score> list1 = new ArrayList<>();
        list1.add(scoreDAO.getScoreById(3));

        List<Score> list2 = new ArrayList<>();
        list2.add(scoreDAO.getScoreById(10));
        list2.add(scoreDAO.getScoreById(2));

        List<Score> list3 = new ArrayList<>();
        list3.add(scoreDAO.getScoreById(8));
        list3.add(scoreDAO.getScoreById(1));
        list3.add(scoreDAO.getScoreById(4));
        list3.add(scoreDAO.getScoreById(9));
        list3.add(scoreDAO.getScoreById(7));
        list3.add(scoreDAO.getScoreById(5));
        list3.add(scoreDAO.getScoreById(6));

        assertEquals(scoreDAO.getTenBestScoresPerLevel(1), list1);
        assertEquals(scoreDAO.getTenBestScoresPerLevel(2), list2);
        assertEquals(scoreDAO.getTenBestScoresPerLevel(3), list3);
    }

    /**
     * Test 7
     * Checking if the getNumberOfGamesPlayedByPlayerName method works well or not
     */
    @Test
    void testGetNumberOfGamesPlayedByPlayerName() throws ScoreException {
        assertEquals(5, scoreDAO.getNumberOfGamesPlayedByPlayerName("Player 5"));
        assertEquals(3, scoreDAO.getNumberOfGamesPlayedByPlayerName("Player 4"));
    }
}