-- dropping tables
DROP TABLE IF EXISTS Scores;
DROP TABLE IF EXISTS Levels;

-- creating tables
CREATE TABLE Levels(
    id           INT         NOT NULL IDENTITY,
    levelName    VARCHAR(45) NOT NULL,
    lowNumber    INT         NOT NULL,
    highNumber   INT         NOT NULL,
    numOfDigits  INT         NOT NULL,
    allowDoubles TINYINT     NOT NULL,

    CONSTRAINT PK_Level PRIMARY KEY (id)
       -- UNIQUE INDEX id_UNIQUE (id ASC)
);

CREATE TABLE Scores
(
    id              INT         NOT NULL IDENTITY,
    playerName      VARCHAR(45) NOT NULL,
    numberOfGuesses INT         NOT NULL,
    gamesPlayed     INT         NOT NULL,
    timePlayed      VARCHAR(45) NOT NULL,
    scoreLevelId    INT         NOT NULL,

    CONSTRAINT PK_Score PRIMARY KEY (id)
    -- UNIQUE INDEX id_UNIQUE (id ASC)
);