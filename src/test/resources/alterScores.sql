-- adding foreign key
ALTER TABLE Scores
    ADD CONSTRAINT fk_scoreLevelId_Levels
        FOREIGN KEY (scoreLevelId)
            REFERENCES Levels (id)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION;