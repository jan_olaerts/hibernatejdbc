-- inserting data into tables
INSERT INTO Levels(id, levelName, lowNumber, highNumber, numOfDigits, allowDoubles) VALUES (1, 'Normal', 1, 6, 4, 0);
INSERT INTO Levels(id, levelName, lowNumber, highNumber, numOfDigits, allowDoubles) VALUES (2, 'Advanced', 1, 8, 6, 0);
INSERT INTO Levels(id, levelName, lowNumber, highNumber, numOfDigits, allowDoubles) VALUES (3, 'Expert', 0, 9, 6, 1);

INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (1, 'Player 1', 5, 1, '00:01:17', 3);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (2, 'Player 2', 10, 1, '00:02:17', 2);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (3, 'Player 3', 15, 1, '00:01:34', 1);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (4, 'Player 4', 7, 2, '00:01:56', 3);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (5, 'Player 4', 13, 3, '00:02:41', 3);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (6, 'Player 4', 21, 1, '00:01:12', 3);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (7, 'Player 4', 12, 2, '00:00:18', 3);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (8, 'Player 2', 1, 3, '00:01:17', 3);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (9, 'Player 3', 9, 4, '00:02:45', 3);
INSERT INTO Scores(id, playerName, numberOfGuesses, gamesPlayed, timePlayed, scoreLevelId) VALUES (10, 'Player 5', 3, 5, '00:03:00', 2);